define([], function() {

  function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }


  return shuffleArray([
    {
      id: 0,
      question: "Where is the restaurant you last went out to?"
    },
    {
      id: 1,
      question: "Where did you last go on vacation?"
    },
    {
      id: 2,
      question: "Where do you get most of your work done?"
    },
    {
      id: 3,
      question: "Where is the grocery store you most frequently visit?"
    },
    {
      id: 4,
      question: "Where does your best friend live?"
    },
    {
      id: 5,
      question: "Where was your mother born?"
    },
    {
      id: 6,
      question: "Where did you live when you were a toddler?"
    },
    {
      id: 7,
      question: "Where did you go to high school?"
    },
    {
      id: 8,
      question: "Where were you on New Year's eve of 2013?"
    },
    {
      id: 11,
      question: "Where is the city hall of the town you currently live?"
    },
    {
      id: 12,
      question: "Where was the first vacation you remember?"
    },
    {
      id: 13,
      question: "Where have you not been yet, but would you like to travel to?"
    },
    {
      id: 14,
      question: "Where was the furthest place from home you have been?"
    },
    {
      id: 15,
      question: "Where did your father live when he was a teenager?"
    }

  ]);


});