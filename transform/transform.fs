﻿module transform

open System

//sessions
type Session = {question: decode.QuestionEntry; entries: decode.LogEntry[]}

let newSession question  = {question=question; entries=[||]}
let updateSession session entry =
    let newentries = Array.append session.entries [|entry|]
    {question=session.question;entries=newentries} 

let addEntry sessions entry = 
    match sessions with
        | h::t -> List.Cons((updateSession h entry), t)
        | _ -> sessions

let foldSession accumulator (entry : decode.LogEntry option) = 
    match entry with
        | Some(decode.Question(x)) -> List.Cons (newSession x, accumulator)
        | Some(x) -> addEntry accumulator x
        | None -> accumulator
            
let GetSessions entries = 
    List.fold foldSession [] entries


//movements
type Position = {transformation: decode.Transformation; time: float}
    
type Movement(f, t) = 
    member this.f = f
    member this.t = t
    member this.duration = this.t.time - this.f.time
    member this.sx_ratio = this.t.transformation.sx / this.f.transformation.sx
    member this.sy_ratio = this.t.transformation.sy / this.f.transformation.sy
    member this.dtx = this.t.transformation.tx - this.f.transformation.tx
    member this.dty = this.t.transformation.ty - this.f.transformation.ty
    member this.dtx_pix = this.t.transformation.tx - (this.f.transformation.tx * this.t.transformation.sx / this.f.transformation.sx)
    member this.dty_pix = this.t.transformation.ty - (this.f.transformation.ty * this.t.transformation.sy / this.f.transformation.sy)
    member this.ds =  Math.Sqrt(this.sx_ratio * this.sy_ratio)
    member this.mvtype = 
        if this.f.transformation.sx = this.t.transformation.sx then
            "pan"
        else 
            "zoom"
    member this.changes = (this.mvtype,this.ds,this.dtx_pix,this.dty_pix,this.duration)
    member this.interpolate timestep = 

        let mutable interpolatedMoves = [||]
     

        if (this.duration <= timestep) then
            interpolatedMoves <- Array.append interpolatedMoves [|this|]
        else 
            
            let mutable frompos = this.f
            let scalediff = this.t.transformation.sx - this.f.transformation.sx
            let xtranslationdiff = this.t.transformation.tx - this.f.transformation.tx
            let ytranslationdiff = this.t.transformation.ty - this.f.transformation.ty

       
            let fromtime = this.f.time
            let totime = this.t.time
            let timediff = this.duration
            let mutable time = fromtime + timestep

            

            while time < totime do
                let fraction = (time - fromtime) / timediff
                let newscale = this.f.transformation.sx + scalediff * fraction
                let newxt = this.f.transformation.tx + xtranslationdiff * fraction
                let newyt = this.f.transformation.ty + ytranslationdiff * fraction
                let newtrans:decode.Transformation = {sx=newscale;sy=newscale;tx=newxt;ty=newyt};
                let newpos = {transformation=newtrans;time=time}
                let newmovement = Movement(frompos,newpos)
                interpolatedMoves <- (Array.append interpolatedMoves [|newmovement|])
                frompos <- newpos
                time <- time + timestep
        
            
            //add the last one
            let last = interpolatedMoves.[interpolatedMoves.Length - 1]
            interpolatedMoves <- (Array.append interpolatedMoves [|Movement(last.t,this.t)|])
        
        interpolatedMoves

            

               

let MakePosition (sh:decode.Log) = 
    {transformation=sh.transformation;time=sh.header.time}

let MakeMovement f t  = 
    let flog = decode.GetLog f
    let tlog = decode.GetLog t
    Movement(MakePosition flog, MakePosition tlog)
    
    
let rec FindMovements entryList acc = 
    match entryList with
        | (decode.PanStart(_) as ps)::(decode.PanEnd(_) as pe)::t -> FindMovements t (Array.append acc [|MakeMovement ps pe|])
        | (decode.ZoomStart(_) as zs)::(decode.ZoomStart(_) as zs2)::t -> FindMovements (zs2::t) (Array.append acc [|MakeMovement zs zs2|])
        | (decode.ZoomStart(_) as zs)::(decode.AnimationEnd(_) as ae)::t -> FindMovements (t) (Array.append acc [|MakeMovement zs ae|])
        | _ -> acc

let movementBeginEnd entry =
    match entry with
        | decode.AnimationEnd(x) ->  true
        | decode.ZoomStart(x) -> true
        | decode.PanEnd(x) -> true
        | decode.PanStart(x) -> true
        | _ -> false

let FindMoves entries = 
    let entryList = entries 
                        |> Array.filter movementBeginEnd
                        |> Array.toList
    FindMovements entryList [||]



    
