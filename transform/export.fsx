﻿#r "../packages/FSharp.Data.2.0.5/lib/net40/FSharp.Data.dll"

#load "decode.fs"
#load "transform.fs"
#load "parse.fs"
#load "export.fs"

open System.IO


let outputdir = @"D:/MyThesis/data/"
let inputdir = @"D:/MyThesis/logs_trim/" 
let dir = new DirectoryInfo(inputdir)
let logfiles = dir.GetFiles() 
                    |> Array.map (fun (file) -> (file,decode.ParseDocument(file.FullName)))


let ks = [1 ..2 .. 10]
let vars = [export.Variable.Scale;export.Variable.TranslationX;export.Variable.TranslationY]
let interpoltime= [56.0] 

for k in ks
    do 
        for var in vars
            do
                for interpol in interpoltime
                    do
                        let outfile = outputdir + "input_" + k.ToString() + "_" + (export.VariableToString var) + "_" + interpol.ToString() + ".csv"
                        export.exportKTimeSeries k var interpol logfiles outfile                        
                    


