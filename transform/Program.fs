﻿module Program

open FSharp.Data
open System.IO

[<EntryPoint>]
let main argv =
 
    printfn "%A" argv

    let k = 1
    let v = export.Variable.Scale
    let outfile = @"D:/tmp/interped_" + k.ToString() + "_" + (export.VariableToString v) + ".csv"
    let inputdir = @"D:/tmp/test1" 
    export.exportKTimeSeries k export.Variable.Scale inputdir outfile



    0 // return an integer exit code