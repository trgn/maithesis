﻿module parse

open System.IO
open System

let interpolationTimeStep = 10

let rec flatten (list:System.Collections.IEnumerable) =
    [for x in list do
        match x with
        | :? System.Collections.IEnumerable as s -> yield! flatten(s)
        | _ -> yield x
    ]

type Session(session, movements) = 
    member this.session:transform.Session = session
    member this.movements = movements
    member this.deltas = Array.map (fun (m:transform.Movement) -> m.changes) this.movements
    member this.interpolatedMovements interpolationDuration  = 
        Array.collect (fun (mov:transform.Movement) -> (mov.interpolate interpolationDuration)) this.movements
    member this.kMoves k interpolationDuration = 
        let moveSeq = Array.toSeq (this.interpolatedMovements interpolationDuration)
        Seq.toArray (Seq.windowed k moveSeq)                  

                
type Trial(file:FileInfo, sessions) =
    member this.trialId = file.Name
    member this.sessions = sessions
    member this.allDeltas = Array.map (fun (s:Session) -> s.deltas) this.sessions
    member this.allKMoves k interp = Array.map (fun (s:Session) -> s.kMoves k interp) this.sessions
    member this.kMovesTabular k interp =
        let kmoves = this.allKMoves k interp
        Array.collect Operators.id kmoves


               
            
let toTrial (file:FileInfo,logs:FSharp.Data.JsonValue[]) = 
    
    let decoded = Array.map decode.DecodeEntry logs
    let sessions = transform.GetSessions (Seq.toList decoded)
    let sessionsArray = List.toArray sessions

    let mvs = sessionsArray 
                |> Array.map (fun s -> s.entries) 
                |> Array.map transform.FindMoves
    
    let sess = Array.zip sessionsArray mvs
                    |> Array.map (fun((a,b:transform.Movement[]))->Session(a,b))
    
    Trial(file, sess)








    