﻿module decode

open FSharp.Data
open FSharp.Data.JsonExtensions

//utils
let unpack logs =
    match logs with
    | JsonValue.Array (items) -> items
    | x -> [|x|]

let ParseDocument (doc : string) : JsonValue[] =
    let json = JsonValue.Load(doc)
    unpack json

//types
type LogHeader = { time : float; typ: string}
type Transformation = {sx: float; sy: float; tx: float; ty: float;}
type Tile = {xLevel:float;yLevel: float;x:  float;y:float}

type Log = {header: LogHeader; transformation: Transformation; tile:Tile;}

type Question = {question:int; trialId:int}
type Location = {x:float;y:float}


type QuestionEntry = {log:Log; question:Question}   

type PanStartEntry =  {log: Log}
type PanEndEntry = {log:Log}
type TransformationChangeEntry = {log: Log}
type ZoomStartEntry = {log: Log} 
type AnimationEndEntry = {log: Log}
type ConfirmEntry = {log: Log}
type DropIconEntry = {log: Log; location:Location; question: Question}

type LogEntry =
    | Question of QuestionEntry
    | PanStart of PanStartEntry
    | PanEnd of PanEndEntry
    | TransformationChange of TransformationChangeEntry
    | ZoomStart of ZoomStartEntry
    | AnimationEnd of AnimationEndEntry
    | Confirm of ConfirmEntry
    | DropIcon of DropIconEntry
    

//decoders
let DecodeHeader json = {time = (json?("time").AsFloat()); typ=(json?("type").AsString())}

let DecodeTransformation json = 
    let t = json?transformation
    {sx=(t?sx).AsFloat(); sy=(t?sy).AsFloat(); tx=(t?tx).AsFloat();ty=(t?ty).AsFloat()}

let DecodeTile json = 
    let t = json?tile
    {xLevel=(t?xLevel).AsFloat(); yLevel=(t?yLevel).AsFloat(); x=(t?x).AsFloat();y=(t?y).AsFloat()}

let DecodeQuestion questionJson =  
    {question=questionJson?question.AsInteger();trialId=questionJson?trialId.AsInteger()}

let DecodeLocation l = {x=(l?x).AsFloat();y=(l?y).AsFloat()}

let DecodeLog json = 
    {header= DecodeHeader json;transformation = DecodeTransformation json; tile = DecodeTile json;}

let DecodeQuestionEntry json : QuestionEntry = {log=DecodeLog json;question= DecodeQuestion json?("object")}

let DecodePanStartEntry json : PanStartEntry=  {log=DecodeLog json}

let DecodePanEndEntry json : PanEndEntry = {log=DecodeLog json}

let DecodeTransformationChange json : TransformationChangeEntry =  {log= DecodeLog json}

let DecodeZoomStart json : ZoomStartEntry = {log=DecodeLog json}

let DecodeAnimationEnd json : AnimationEndEntry = {log=DecodeLog json}

let DecodeConfirm json : ConfirmEntry = {log = DecodeLog json}

let DecodeDropIcon json : DropIconEntry = {log=DecodeLog json; location=DecodeLocation (json?("object"))?location; question=DecodeQuestion (json?("object")?question)}

let DecodeEntry json : LogEntry option = 
    let lh = DecodeHeader json
    match lh.typ with
        | "panStart" -> Some(LogEntry.PanStart(DecodePanStartEntry json))
        | "panEnd" -> Some(LogEntry.PanEnd(DecodePanEndEntry json))
        | "question" -> Some(LogEntry.Question(DecodeQuestionEntry json))
        | "transformationChange" -> Some(LogEntry.TransformationChange(DecodeTransformationChange json))
        | "zoomInStart" -> Some(LogEntry.ZoomStart(DecodeZoomStart json))
        | "animationEnd" -> Some(LogEntry.AnimationEnd(DecodeAnimationEnd json))
        | "confirm" -> Some(LogEntry.Confirm(DecodeConfirm json))
        | "dropIcon" -> Some(LogEntry.DropIcon(DecodeDropIcon json))
        | _ -> None     


let GetLog entry  = 
    match entry with
        | LogEntry.PanStart(x) -> x.log
        | LogEntry.PanEnd(x) -> x.log
        | LogEntry.Question(x) -> x.log
        | LogEntry.TransformationChange(x) -> x.log
        | LogEntry.ZoomStart(x) -> x.log
        | LogEntry.AnimationEnd(x) -> x.log
        | LogEntry.Confirm(x) -> x.log
        | LogEntry.DropIcon(x) -> x.log
       

    


                     
