﻿module export


open FSharp.Data
open FSharp.Data.JsonExtensions
open System.IO
open Microsoft.FSharp.Reflection;

let tupleToString t = 
  if FSharpType.IsTuple(t.GetType()) then
    FSharpValue.GetTupleFields(t)
    |> Array.map string
    |> String.concat ", "
  else failwith "not a tuple!"

let dump lines outfile = 
    use wr = File.CreateText(outfile)
    Array.iter (fun (line:string) -> wr.WriteLine(line)) lines


let toArray logfiles k interp =
    let trials = Array.map parse.toTrial logfiles
    let allMoves = Array.map (fun (t:parse.Trial) -> t.kMovesTabular k interp) trials
    let moveRows = Array.collect Operators.id allMoves
    let mapRow (t:transform.Movement[]) = 
        let row = Array.map (fun (a:transform.Movement) -> a.changes) t
        let collectTups acc tup = 
            Array.append acc [|tup|]
        Array.fold collectTups [||] row
    Array.map mapRow moveRows

type Variable = 
    | Scale
    | TranslationX
    | TranslationY


let VariableToString v = match v with
                            | Variable.Scale -> "scale"
                            | Variable.TranslationX -> "translationX"
                            | Variable.TranslationY -> "translationY"

let exportKTimeSeries k variable interp logfiles outfile = 
    let rawLines = toArray logfiles (k + 1) interp
    let stripPZ tup = 
        let (px,s,x,y,d) = tup
        (s,x,y,d)
    let stripAll ar = Array.map stripPZ ar
    let getFirsts (a:(string * float * float * float *float)[]) = a.[0..a.Length-2]
    let previousK = Array.map getFirsts rawLines |> Array.map stripAll
    let getLast (a:(string * float * float * float *float)[]) = a.[a.Length - 1]
    let keepVar (s,x,y,d) = match variable with
                                | Variable.Scale -> s
                                | Variable.TranslationX -> x
                                | Variable.TranslationY -> y
    let predict = Array.map getLast rawLines |> Array.map stripPZ |> Array.map keepVar
    let predictStrings = Array.map string predict

    let toString lineArray =  lineArray 
                                |> Array.map tupleToString 
                                |> Array.reduce (fun a b -> a + ", " + b)
    let previousKStrings = Array.map toString previousK

    let packTogether (preK,var) = preK + ", " + var
    let all = Array.zip previousKStrings predictStrings |> Array.map packTogether
    dump all outfile