%read input matrix
M = csvread('D:\MyThesis\data\input_5_scale_64.csv');
M = M';

%get inputs and targets
s = size(M);
s = s(1);
i = s - 1;
X = M(1:i,:);
T = M(s,:);


%network
nr_neurons = 20;
trainfunc = 'trainlm';
net = feedforwardnet(nr_neurons,trainfunc);
net = train(net,X,T);

net.trainParam.epochs = 1000;
net.layers{1}.transferFcn = 'logsig';
net.layers{2}.transferFcn = 'purelin';
 
net.performFcn = 'mse';


